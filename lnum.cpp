/* 
 * File:   lnum.cpp
 * Author: Aleksey Lobanov <alekseylobanov1 at gmail.com>
 * 
 * Created on 4 ноября 2014 г., 1:03
 */

#include "lnum.hpp"

template <typename T = long>
T StrToNum(const std::string& Text)//Text not by const reference so that the function can be used with a 
{ //character array as argument
    std::stringstream ss(Text);
    T result;
    return ss >> result ? result : 0;
}

template <typename T = long>
std::string NumtoStr(T Number) {
    std::stringstream ss;
    ss << Number;
    return ss.str();
}

const std::string Lnum_sn::__DIGITS = "0123456789abcdefghijklmnopqrstuvwxyz";

Lnum_sn::Lnum_sn(): Lnum_sn(10) {};

Lnum_sn::Lnum_sn(long base): __base(base), __negative(false) {
    __digs.resize(1);
    __digs.at(0) = 0;
};

Lnum_sn::Lnum_sn(long num, long base): __base(base) {
    if (num == 0)
        Lnum_sn(base);
    else{
        if (num < 0){
            __negative = true;
            num = -num;
        } else
            __negative = false;
        long i = 0;
        long m = 1;
//        !!
        while (num >= __base * m){
            i++;
            m *= __base;
        }
        long sz = i + 1;
        __digs.resize(sz);
        while (i >= 0){
            __digs.at(i) = num / m;
            num = num % m;
            m = m / __base;
            i--;
        }
    }
}

Lnum_sn::Lnum_sn(const std::string s, long base): __base(base), __negative(false) {
    if ((base >= 2) || (base <= __DIGITS.size())) {
        __digs.resize(s.size());
        for (long i = 0; i < s.size(); ++i) {
            long t = __DIGITS.find(s.at(s.size() - 1 - i));
            if ((t >= base) || (t < 0))
                throw std::invalid_argument(std::string("Invalid argument in constructor: ") + s);
            else
                __digs.at(i) = t;
        }
        normalize();
    }
}

Lnum_sn::~Lnum_sn() {
};

Lnum_sn Lnum_sn::operator-() const{
    Lnum_sn temp = *this;
    if (!temp.iszero())
        temp.__negative = !temp.__negative;
    return temp;
}

Lnum_sn Lnum_sn::operator-(const Lnum_sn& a) const{
    Lnum_sn k = a;
    k = -k;
    return ((*this) + k);
}

Lnum_sn Lnum_sn::operator+(const Lnum_sn& a) const{
    if (__base != a.__base) throw std::invalid_argument("Different bases!");
    Lnum_sn temp = Lnum_sn(__base);
    temp.__digs.resize(std::max(size(), a.size()));
    if ( !(__negative xor a.__negative) ) {
        long next = 0;
        for (long i = 0; ((next != 0) || (i < std::max(size(), a.size()))); ++i) {
            temp.setDig(i, (getDig(i) + a.getDig(i) + next) % __base);
            next = (getDig(i) + a.getDig(i) + next) / __base;
        }
        temp.normalize();
        temp.__negative = __negative;
        return temp;
    } else {
        for (long i = 0; i < std::max(size(), a.size()); ++i) {
            if (__negative)
                temp.setDig(i, -getDig(i) + a.getDig(i));
            else
                temp.setDig(i, getDig(i) - a.getDig(i));
        }
        temp.normalize();
        if (temp.getDig(temp.size() - 1) < 0) {
            temp.__negative = true;
            for (long i = 0; i < temp.size(); ++i) {
                temp.setDig(i, -temp.getDig(i));
            }
        }
        for (long i = 0; i < temp.size(); ++i) {
            if (temp.getDig(i) < 0) {
                temp.setDig(i + 1, temp.getDig(i+1) - 1);
                temp.setDig(i, temp.__base + temp.getDig(i));
            }
        }
        temp.normalize();
    }
}

bool Lnum_sn::iszero() const{
    return ((__digs.at(0) == 0) && (size() == 1));
}

std::vector<short>::size_type Lnum_sn::size() const {
    return __digs.size();
}

short Lnum_sn::getDig(long n) const {
    if (n > (size() - 1))
        return 0;
    else
        return __digs.at(n);
}

void Lnum_sn::setDig(long n, short val) {
    if (n > (size() - 1)) {
        if (val != 0){
            __digs.resize(n + 1);
            __digs.at(n) = val;
        }
    } else {
        __digs.at(n) = val;
        if ((val == 0) && n == (size() - 1)){
            normalize();
        }
    }
}

void Lnum_sn::normalize() {
    long rem = 0;
    while (rem < __digs.size()) {
        if ((size() - 1 - rem) <= 0) {
            break;
        } else {
            if (__digs.at(size() - 1 - rem) == 0) {
                ++rem;
            } else {
                break;
            }
        }
    };
    if (rem == __digs.size() - 1) {
        __digs.resize(1);
        __negative = false;
    } else {
        __digs.resize(__digs.size() - rem);
    }
}

const std::string Lnum_sn::toStr() const {
    std::string res;
    res.resize(__negative ? size() + 1 : size());
    if (bool(__negative)) {
        res.at(0) = '-';
        for (long i = 0; i < size(); ++i) {
            res.at(i + 1) = __DIGITS.at(__digs.at(size() - i - 1));
        }
    } else {
        for (long i = 0; i < size(); ++i) {
            res.at(i) = __DIGITS.at(__digs.at(size() - i - 1));
        }
    }
    return res;
}

std::ostream& operator<<(std::ostream& out, const Lnum_sn& n){
    return (out << n.toStr());
}

Lnum_sn Lnum_sn::operator*(const Lnum_sn& a) const{
    if (__base != a.__base) throw std::invalid_argument("Different bases!");
    if (iszero() || a.iszero()) return Lnum_sn(__base);
    Lnum_sn temp;
    temp.__negative = a.__negative xor __negative;
    return temp;
}

Lnum_sn Lnum_sn::operator/(const Lnum_sn& a) const{
    if (__base != a.__base) throw std::invalid_argument("Different bases!");
    Lnum_sn temp;
    return temp;
}

Lnum_sn Lnum_sn::operator%(const Lnum_sn& a) const{
    if (__base != a.__base) throw std::invalid_argument("Different bases!");
    Lnum_sn temp;
    return temp;
}

std::pair<Lnum_sn, Lnum_sn> Lnum_sn::div(const Lnum_sn& a) const {
    if (a.iszero()){
        throw(std::invalid_argument("Division by zero"));
    }else{
        
    }
        
}

bool Lnum_sn::operator==(const Lnum_sn& a) const{
    Lnum_sn t = (*this) - a;
    return t.iszero();
}

bool Lnum_sn::operator!=(const Lnum_sn& a) const {
    Lnum_sn t = (*this) - a;
    return ( !t.iszero() );
}

bool Lnum_sn::operator<(const Lnum_sn& a) const{
    Lnum_sn t = (*this) - a;
    return (t.__negative && !t.iszero());
}

bool Lnum_sn::operator<=(const Lnum_sn& a) const{
    Lnum_sn t = (*this) - a;
    return (t.__negative || t.iszero());
}

bool Lnum_sn::operator>(const Lnum_sn& a) const{
    Lnum_sn t = a - (*this);
    return (t.__negative && !t.iszero());
}

bool Lnum_sn::operator>=(const Lnum_sn& a) const{
    Lnum_sn t = a - (*this);
    return (t.__negative || t.iszero());
}

void Lnum_sn::setBase(long newBase) {
    if (newBase != __base) {
        Lnum_sn temp(newBase);
        temp.__negative = __negative;
        for (long i = 0; i < size(); ++i)
            temp = temp + Lnum_sn(getDig(i)) * Lnum_sn(__base).pow(i);
        (*this) = temp;
    }
}

Lnum_sn Lnum_sn::pow(long n) const {
    Lnum_sn temp(*this);
    for (long i = 0; i < n-1; ++i)
        temp = temp * (*this);
}

