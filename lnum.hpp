/* 
 * File:   lnum.hpp
 * Author: Aleksey Lobanov <alekseylobanov1 at gmail.com>
 *
 * Created on 4 ноября 2014 г., 1:03
 */

#ifndef LNUM_HPP
#define	LNUM_HPP

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <utility>
#include <stdexcept>
#include <limits>

class Lnum_sn {
public:
    Lnum_sn();
    Lnum_sn(long base);
    Lnum_sn(long num, long base);
    Lnum_sn(const std::string s, long base);
    virtual ~Lnum_sn();
    Lnum_sn operator+(const Lnum_sn& a) const;
    Lnum_sn operator-(const Lnum_sn& a) const;
    Lnum_sn operator*(const Lnum_sn& a) const;
    Lnum_sn operator/(const Lnum_sn& a) const;
    Lnum_sn operator%(const Lnum_sn& a) const;
    bool operator>(const Lnum_sn& a) const;
    bool operator>=(const Lnum_sn& a) const;
    bool operator<(const Lnum_sn& a) const;
    bool operator<=(const Lnum_sn& a) const;
    bool operator==(const Lnum_sn& a) const;
    bool operator!=(const Lnum_sn& a) const;
    Lnum_sn operator-() const;
    std::vector<short>::size_type size() const;
    Lnum_sn pow(long n) const;
    void setBase(long newBase);
    bool iszero() const;
    //returns quotient and remainder
    std::pair<Lnum_sn, Lnum_sn> div(const Lnum_sn& a) const;
    const std::string toStr() const;
    template <typename Num>
    Num toNum() const;
    friend std::ostream& operator<<(std::ostream& out, const Lnum_sn& n);
protected:
    bool __negative;
    long __base;
    short getDig(long n) const; // n from [0..size-1]
    void setDig(long n, short val);
    void normalize();
    std::vector<short> __digs;
private:
    static const std::string __DIGITS;
};

std::ostream& operator<<(std::ostream& out, const Lnum_sn& n);

template <typename T = long>
std::string NumtoStr(T Number);

template <typename T = long>
T StrToNum(const std::string& Text);

template <typename Num>
Num Lnum_sn::toNum() const {
    Num x = 1;
    Num res = 0;
    for (long i = 0; i < size(); ++i, x *= __base) {
        res += getDig(i) * x;
    }
    return res;
}

#endif	/* LNUM_HPP */

