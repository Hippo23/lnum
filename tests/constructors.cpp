/* 
 * File:   constructors.cpp
 * Author: Aleksey Lobanov <alekseylobanov1 at gmail.com>
 *
 * Created on 04.11.2014, 14:48:12
 */

#include <stdlib.h>
#include <iostream>
#include "lnum.hpp"

void test1() {
    std::cout << "constructors test 1" << std::endl;
}

void test2() {
    std::cout << "constructors test 2" << std::endl;
    std::cout << "%TEST_FAILED% time=0 testname=test2 (constructors) message=error message sample" << std::endl;
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% constructors" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% test1 (constructors)" << std::endl;
    test1();
    std::cout << "%TEST_FINISHED% time=0 test1 (constructors)" << std::endl;

    std::cout << "%TEST_STARTED% test2 (constructors)\n" << std::endl;
    test2();
    std::cout << "%TEST_FINISHED% time=0 test2 (constructors)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

