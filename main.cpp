#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cmath>
#include "lnum.hpp"

using namespace std;

void testSum(string k, string m){
    Lnum_sn a(k, 10);
    Lnum_sn b(m, 10);
    Lnum_sn c = a + b;
    std::cout << a << " + " << b << " = " << c << " c.size() = " << c.size() << "\n";
}

Lnum_sn fib(long n){
    Lnum_sn t(10), a(10), b("1", 10);
    for (long i = 0; i < n; ++i){
        t = a + b;
        b = a;
        a = t;
    }
    return a;
}    

void testDiff(string k, string m) {
    Lnum_sn a(k, 10);
    Lnum_sn b(m, 10);
    Lnum_sn c = a - b;
    std::cout << a << " - " << b << " = " << c << " c.size() = " << c.size() << "\n";
}

int main(int argc, char** argv) {
    testSum("100", "37");
    testSum("57", "194");
    testSum("194", "57");
    testDiff("78", "45");
    testDiff("57", "194");
    testDiff("194", "57");
    testDiff("10000", "57");
    //cout << "Fib(1000) = " << fib(1000) << '\n';
    cout << Lnum_sn(8,2) << ' ' << Lnum_sn(567891, 10) << ' ' << Lnum_sn(0)
            << ' ' << Lnum_sn(-784,10) << ' ';
    return 0;
}

